DBRelease Images for the AtlasOffline/Athena/AthSimulation Projects.
======================================

This procedure can be used to add a DBRelease package to an image with an existing
full installation of AtlasOffline, Athena or AthSimulation. 

Move to the DBRelease folder
----
```bash
cd docker/dbrelease
```

Build the images
----
To install a given database release package (e.g., DBRelease-XXX.Y.Z) to an existing release image (e.g., atlas/athena:22.0.47), execute:

```bash
docker build --network host -t atlas/athena:22.0.47_XXX.Y.Z --build-arg BASEIMAGE=atlas/athena:22.0.47 --build-arg DBRELEASE=XXX.Y.Z .
```
