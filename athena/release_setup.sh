# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Environment configuration file for the user. Can be sourced to set up
# the release installed in the image.
#

export INSTALLATION_ROOT=/usr
export SITEROOT=${INSTALLATION_ROOT}
export G4PATH=/usr/Geant4
export VO_ATLAS_SW_DIR=/usr
export CALIBPATH=/usr/database/GroupData

for project in \
        "Athena" \
        "AthSimulation" \
        "AtlasOffline" 
do
    if [ -d "/usr/$project" ]; then
       PROJECT=$project
       RELEASE=$(\cd /usr/$project/;\ls)
       BINARY_TAG=$(\cd /usr/$project/${RELEASE}/InstallArea/;\ls)
    fi
done

export LCG_PLATFORM=${BINARY_TAG}

# Set up the compiler:
source /opt/lcg/gcc/*/x86_64-*/setup.sh
echo "Configured GCC from: ${CC}"

if [ -d "/usr/database/DBRelease" ]; then
   export ATLAS_DB_AREA=/usr/database
fi

# LCG:
export LCG_RELEASE_BASE=/opt/lcg
echo "Taking LCG releases from: ${LCG_RELEASE_BASE}"

# Set up the release:
source ${INSTALLATION_ROOT}/${PROJECT}/${RELEASE}/InstallArea/${BINARY_TAG}/setup.sh
echo "Configured ${PROJECT} from: ${INSTALLATION_ROOT}/${PROJECT}/${RELEASE}/InstallArea/${BINARY_TAG}"

# Include patches
PATCH_PATH=/opt/build/${BINARY_TAG}
PATCH_SETUP=${PATCH_PATH}/setup.sh
if [ -f "$PATCH_SETUP" ]; then
  echo "Including local patches at " $PATCH_PATH
  source $PATCH_SETUP
fi

# Unset FRONTIER_SERVER
unset FRONTIER_SERVER
