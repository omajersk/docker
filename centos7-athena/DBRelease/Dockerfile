## Add ALRB and DBRelease to fat container

# Make the base image configurable.
ARG BASEIMAGE=atlas/athena:22.0.6_2019-10-04T2129
FROM ${BASEIMAGE}

ARG DBRELEASE=31.8.1
ENV DBRELEASE_INSTALLDIR=/cvmfs/atlas.cern.ch/repo/sw/database
ENV TNS_ADMIN=${DBRELEASE_INSTALLDIR}/DBRelease/${DBRELEASE}/oracle-admin
ENV DATAPATH=${DBRELEASE_INSTALLDIR}/DBRelease/${DBRELEASE}

USER root
WORKDIR /root

RUN mkdir -p /cvmfs/atlas-condb.cern.ch/repo/conditions && \
    mkdir -p /cvmfs/sft.cern.ch && \
    mkdir -p /cvmfs/atlas.cern.ch/repo/sw/software && \
    mkdir -p /cvmfs/atlas-nightlies.cern.ch/repo/sw && \
    mkdir -p $DBRELEASE_INSTALLDIR && \
    adduser cvmfs && chmod 755 /home/cvmfs && \
    chown -R cvmfs:cvmfs /cvmfs && \
    mkdir -p /usr/sw/lcg && ln -s /opt/lcg /usr/sw/lcg/releases

# Add configuration file for steering AtlasSetup
COPY setup /setup.sh
COPY release_setup /release_setup.sh
COPY motd /etc/

RUN echo "source \$AtlasSetup/scripts/asetup.sh ${PROJECT},${RELEASE} --cmakeversion=current" >> /release_setup.sh && \
    echo "      source \$AtlasSetup/scripts/asetup.sh ${PROJECT},${RELEASE} --cmakeversion=current" >> /etc/motd && \
    echo "source /setup.sh " >> /release_setup.sh
         
USER cvmfs
WORKDIR /home/cvmfs

# Add script to set up path to cmake
COPY cmake_setup cmake_setup.sh

# Install DBRelease
RUN wget http://atlas.bu.edu/~youssef/pacman/sample_cache/tarballs/pacman-latest.tar.gz && \
    tar xf pacman-latest.tar.gz && \
    rm pacman-latest.tar.gz && \
    cd pacman-* && \
    source $PWD/setup.sh && \
    cd $DBRELEASE_INSTALLDIR && \
    pacman -allow trust-all-caches -get http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/pacman4/DBRelease:DBRelease-${DBRELEASE} && \
    cd ${DBRELEASE_INSTALLDIR}/DBRelease && ln -s ${DBRELEASE} current

# Install minimal ALRB
Run git clone https://gitlab.cern.ch/atlas-tier3sw/manageTier3SW.git userSupport/manageTier3SW && \
    cd userSupport/manageTier3SW && \
    ./updateManageTier3SW.sh -a /cvmfs/atlas.cern.ch/repo -i asetup,acm -j && \
    source /home/cvmfs/cmake_setup.sh && \
    ln -s /usr /cvmfs/atlas.cern.ch/repo/sw/software/${RELEASES} && \
    ln -s /sw/atlas /cvmfs/atlas.cern.ch/repo/sw/tdaq
    
USER root

CMD cat /etc/motd && su -l atlas
