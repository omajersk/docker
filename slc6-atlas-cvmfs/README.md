SLC6 ATLAS OS
=============

This configuration can be used to set up a base image for ATLAS
Project builds

You can build it with:

```bash
docker build --network host -t gitlab-registry.cern.ch/atlas-sit/docker/build-athena:latest --build-arg user_id=$(id $(whoami) -u) --build-arg username=$(whoami) --build-arg group_id=$(id $(whoami) -g) --build-arg groupname=zp .
```

Compiler
--------

Note that the image holds GCC out of the box. This is because
production mode ATLAS releases use a compiler set by the asetup 
in /cvmfs/sft.cern.ch/lcg at the moment.
